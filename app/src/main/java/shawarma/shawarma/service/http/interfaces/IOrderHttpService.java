package shawarma.shawarma.service.http.interfaces;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import shawarma.shawarma.data.model.database.order.OrderModel;
import shawarma.shawarma.data.model.database.order.OrderResponseModel;

public interface IOrderHttpService {
    @POST("createOrder")
    Observable<OrderResponseModel> createOrder(@Body OrderModel orderModel);
}
