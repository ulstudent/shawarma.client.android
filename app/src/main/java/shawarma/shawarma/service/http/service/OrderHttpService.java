package shawarma.shawarma.service.http.service;


import io.reactivex.Observable;
import retrofit2.Call;
import shawarma.shawarma.data.model.database.order.OrderModel;
import shawarma.shawarma.data.model.database.order.OrderResponseModel;
import shawarma.shawarma.service.http.base.BaseHttpService;
import shawarma.shawarma.service.http.interfaces.IOrderHttpService;

public class OrderHttpService extends BaseHttpService<IOrderHttpService> {
    public OrderHttpService() {
        super(IOrderHttpService.class);
    }

    public Observable<OrderResponseModel> createOrder(OrderModel orderModel) {
        return service.createOrder(orderModel);
    }
}
