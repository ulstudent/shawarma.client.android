package shawarma.shawarma.service.http.base;

import shawarma.shawarma.service.http.RetrofitService;

public class BaseHttpService<Service> {

    protected Service service;

    public BaseHttpService(Class<Service> httpServiceClass) {
        service = RetrofitService.getInstance().retrofit.create(httpServiceClass);
    }
}
