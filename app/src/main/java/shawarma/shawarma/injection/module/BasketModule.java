package shawarma.shawarma.injection.module;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import shawarma.shawarma.data.model.Basket;
import shawarma.shawarma.data.model.dish.DishUIModel;

@Module
public class BasketModule {

    @Provides
    @NonNull
    @Singleton
    public Basket provideBasket() {
        Basket basket = new Basket();
        DishUIModel dishUIModel = new DishUIModel();
        dishUIModel.setName("Моя шаурмен");
        dishUIModel.setCost(200);
        dishUIModel.setDescription("очень вкусно");
        //dishUIModel.setId("uuua-211312-1-123");

        //basket.addDish(dishUIModel, 2);

        dishUIModel = new DishUIModel();
        dishUIModel.setName("Моя шаурмен123");
        dishUIModel.setCost(200);
        dishUIModel.setDescription("очень вкусно");
        //dishUIModel.setId("uuua-211312-1-123");

        //basket.addDish(dishUIModel, 3);
        return basket;
    }
}
