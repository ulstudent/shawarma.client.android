package shawarma.shawarma.injection.component;


import javax.inject.Singleton;

import dagger.Component;
import shawarma.shawarma.injection.module.BasketModule;
import shawarma.shawarma.ui.itemDetail.ItemDetailPresenter;
import shawarma.shawarma.ui.menu.MenuPresenter;
import shawarma.shawarma.ui.orderDetails.OrderDetailPresenter;
import shawarma.shawarma.ui.orderEditing.OrderEditingPresenter;
import shawarma.shawarma.ui.orderEditing.item.OrderDishItemPresenter;
import shawarma.shawarma.ui.orderEditing.list.OrderDishAdapterPresenter;

@Component(modules = {BasketModule.class})
@Singleton
public interface BasketComponent {
    void inject(MenuPresenter menuPresenter);

    void inject(ItemDetailPresenter itemDetailPresenter);

    void inject(OrderEditingPresenter orderEditingPresenter);

    void inject(OrderDetailPresenter orderDetailPresenter);

    void inject(OrderDishItemPresenter orderDishItemPresenter);

    void inject(OrderDishAdapterPresenter orderDishAdapterPresenter);
}
