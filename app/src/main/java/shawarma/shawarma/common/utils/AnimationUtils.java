package shawarma.shawarma.common.utils;

import android.animation.Animator;
import android.view.View;
import android.view.ViewAnimationUtils;

public class AnimationUtils {
    public static void makeViewAppear(View view) {
// get the center for the clipping circle
        int cx = (view.getLeft() + view.getRight()) / 2;
        int cy = (view.getTop() + view.getBottom()) / 2;

// get the final radius for the clipping circle
        int finalRadius = Math.max(view.getWidth(), view.getHeight());

// create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);

// make the view visible and start the animation
        view.setVisibility(View.VISIBLE);
        anim.start();
    }
}

