package shawarma.shawarma.base;

import com.arellomobile.mvp.MvpAppCompatFragment;

public abstract class MvpFragment extends MvpAppCompatFragment {

    public static final String MAIN_TAG = "MAIN_TAG";
    public static final String ADDITIONAL_TAG = "ADDITIONAL_TAG";

    public abstract String getRouteTag();
}
