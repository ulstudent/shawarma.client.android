package shawarma.shawarma;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import shawarma.shawarma.common.utils.TypefaceUtil;
import shawarma.shawarma.injection.component.BasketComponent;
import shawarma.shawarma.injection.component.DaggerBasketComponent;
import shawarma.shawarma.injection.module.BasketModule;

public class ShawarmaApp extends Application {
    private static BasketComponent basketComponent;

    public static BasketComponent getBasketComponent() {
        return basketComponent;
    }

    private static Context appContext;

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        basketComponent = buildBasketComponent();
        Realm.init(getApplicationContext());

        TypefaceUtil.overrideFont(getApplicationContext(), "fonts/Elegante-Regular.ttf");
    }

    protected BasketComponent buildBasketComponent() {
        return DaggerBasketComponent.builder()
                .basketModule(new BasketModule())
                .build();
    }
}
