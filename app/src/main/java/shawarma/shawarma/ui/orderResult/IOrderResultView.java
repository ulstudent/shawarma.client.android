package shawarma.shawarma.ui.orderResult;

import com.arellomobile.mvp.MvpView;

public interface IOrderResultView extends MvpView {
    void setOrderNumberTv(String number);

    void setText(String name);
}
