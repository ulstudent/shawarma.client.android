package shawarma.shawarma.ui.orderResult;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import io.realm.Realm;
import shawarma.shawarma.data.model.database.order.OrderResponseModel;

@InjectViewState
public class OrderResultPresenter extends MvpPresenter<IOrderResultView> {

    OrderResponseModel orderResponseModel;

    public void setOrderInfo() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            orderResponseModel = realm1.where(OrderResponseModel.class).findFirst();
            getViewState().setOrderNumberTv(orderResponseModel.getHash());
            getViewState().setText(orderResponseModel.getBuyerName());
        });
    }
}
