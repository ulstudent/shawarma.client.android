package shawarma.shawarma.ui.orderResult;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.ui.main.MainActivity;

public class OrderResultActivity extends MvpAppCompatActivity implements IOrderResultView {

    @InjectPresenter
    OrderResultPresenter orderResultPresenter;

    @BindView(R.id.orderNumberTv)
    TextView orderNumberTv;

    @BindView(R.id.textTv)
    TextView textTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_result);
        ButterKnife.bind(this);

        orderResultPresenter.setOrderInfo();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    public void setOrderNumberTv(String number) {
        orderNumberTv.setText(number);
    }

    @Override
    public void setText(String name) {
        textTv.setText(getString(R.string.your_check_number, name));
    }
}
