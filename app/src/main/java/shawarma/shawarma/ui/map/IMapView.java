package shawarma.shawarma.ui.map;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface IMapView extends MvpView {
    void setUpdateLocationBtn();

    void setCurrentLocationBtn();
}
