package shawarma.shawarma.ui.map;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class MapPresenter extends MvpPresenter<IMapView> {
    void setUpdateLocationBtnOnClick() {
        getViewState().setUpdateLocationBtn();
    }

    void setCurrentLocationBtn() {
        getViewState().setCurrentLocationBtn();
    }
}
