package shawarma.shawarma.ui.menu;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import shawarma.shawarma.ShawarmaApp;
import shawarma.shawarma.data.model.Basket;
import shawarma.shawarma.data.model.database.NodeAlias;
import shawarma.shawarma.data.model.database.UserDataModel;
import shawarma.shawarma.data.model.dish.DishUIModel;

@InjectViewState
public class MenuPresenter extends MvpPresenter<IMenuView> {

    @Inject
    Basket basket;

    private List<DishUIModel> dishUIModels = new ArrayList<>();

    private DatabaseReference dishRef = FirebaseDatabase.getInstance()
            .getReference()
            .child(NodeAlias.DISH);

    public MenuPresenter() {
        ShawarmaApp.getBasketComponent().inject(this);
    }

    public boolean checkUserDataIsExists() {
        Realm realm = Realm.getDefaultInstance();
        return !realm.where(UserDataModel.class).findAll().isEmpty();
    }

    //нет проверки на null потому что вызываем после проверки в onCreate
    public String getUserName() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<UserDataModel> userDataModels = realm.where(UserDataModel.class).findAll();
        if (!userDataModels.isEmpty())
            return userDataModels.first().getName();
        else return null;
    }

    private boolean basketIsEmpty() {
        return basket.isEmpty();
    }

    public void setGoToPaymentBtn() {
        getViewState().setGoToPaymentBtn(!basketIsEmpty());
    }

    public void onActivityStart() {
        dishRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dishUIModels.clear();
                for (DataSnapshot dishSnapshot : dataSnapshot.getChildren()) {
                    DishUIModel dishUIModel = dishSnapshot.getValue(DishUIModel.class);
                    dishUIModel.setId(Integer.valueOf(dishSnapshot.getKey()));
                    dishUIModels.add(dishUIModel);
                }
                getViewState().setMenuList(dishUIModels);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void attachView(IMenuView view) {
        super.attachView(view);
        view.setMenuItem();
    }

    public void navigateToLoginIfUserDataEmpty() {
        if (!checkUserDataIsExists()) {
            getViewState().navigateToLogin();
        }
    }
}
