package shawarma.shawarma.ui.menu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.data.model.dish.DishUIModel;

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishViewHolder> {

    private List<DishUIModel> dishUIModelList = new ArrayList<>();

    public void setDishUIModelList(List<DishUIModel> dishUIModelList) {
        this.dishUIModelList = dishUIModelList;
    }

    @Override
    public DishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu, parent, false);
        return new DishViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DishViewHolder holder, int position) {
        final DishUIModel dishUIModel = dishUIModelList.get(position);
        holder.nameText.setText(dishUIModel.getName());
        holder.itemView.setOnClickListener(v -> onItemClicked(String.valueOf(dishUIModel.getId()),
                holder.itemView));
        holder.descriptionText.setText(dishUIModel.getDescription());

        loadImage(dishUIModel.getPictureUrl(), holder.itemImage);
    }

    @Override
    public int getItemCount() {
        return dishUIModelList.size();
    }

    class DishViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView nameText;

        @BindView(R.id.itemImage)
        ImageView itemImage;

        @BindView(R.id.descriptionGradient)
        View descriptionGradient;

        @BindView(R.id.description)
        TextView descriptionText;

        public DishViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public DishUIModel getItemAt(int position) {
        return dishUIModelList.get(position);
    }

    public void onItemClicked(String itemId, View itemView) {
    }

    public void loadImage(String url, ImageView imageView) {
    }
}
