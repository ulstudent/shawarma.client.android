package shawarma.shawarma.ui.menu;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import shawarma.shawarma.data.model.dish.DishUIModel;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface IMenuView extends MvpView {
    void setGoToPaymentBtn(boolean isVisible);

    @StateStrategyType(SkipStrategy.class)
    void navigateToLogin();

    void setMenuItem();

    void setMenuList(List<DishUIModel> dishUIModelList);
}
