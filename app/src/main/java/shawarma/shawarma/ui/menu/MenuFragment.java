package shawarma.shawarma.ui.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import shawarma.shawarma.R;
import shawarma.shawarma.base.MvpFragment;
import shawarma.shawarma.data.model.dish.DishUIModel;
import shawarma.shawarma.ui.itemDetail.ItemDetailActivity;
import shawarma.shawarma.ui.login.LoginActivity;
import shawarma.shawarma.ui.main.MainActivity;
import shawarma.shawarma.ui.orderEditing.OrderEditingPresenter;

import static android.support.v4.app.ActivityOptionsCompat.makeSceneTransitionAnimation;

public class MenuFragment extends MvpFragment implements IMenuView {

    @InjectPresenter
    MenuPresenter menuPresenter;

    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.greeting)
    TextView greetingText;
    @BindView(R.id.goToBasketBtn)
    FloatingActionButton goToBasketBtn;

    private DishAdapter dishAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        menuPresenter.navigateToLoginIfUserDataEmpty();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);

        goToBasketBtn.setImageResource(R.drawable.ic_shopping_basket_white_24dp);

        greetingText.setText(getResources().getString(R.string.greeting, menuPresenter.getUserName()));
        dishAdapter = new DishAdapter() {
            @Override
            public void onItemClicked(String itemId, View itemView) {
                Intent intent = new Intent(getContext(), ItemDetailActivity.class);
                ActivityOptionsCompat options = makeSceneTransitionAnimation((Activity) getContext(),
                        Pair.create(itemView.findViewById(R.id.itemImage),
                                getResources().getString(R.string.transitionNameImage)));
                intent.putExtra(ItemDetailActivity.ITEM_DISH_BUNDLE_KEY,
                        dishAdapter.getItemAt(Integer.valueOf(itemId)));
                startActivity(intent, options.toBundle());
            }

            @Override
            public void loadImage(String url, ImageView imageView) {
                Glide.with(MenuFragment.this)
                        .load(url)
                        .bitmapTransform(new CenterCrop(getContext()),
                                new RoundedCornersTransformation(getContext(), 20, 0))
                        .into(imageView);
            }
        };
        recyclerView.setAdapter(dishAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        menuPresenter.onActivityStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        menuPresenter.setGoToPaymentBtn();
    }

    @Override
    public void setGoToPaymentBtn(boolean isVisible) {
        if (isVisible) {
            goToBasketBtn.setVisibility(View.VISIBLE);
            goToBasketBtn.setOnClickListener(v ->
                    startActivity(new Intent(getContext(), OrderEditingPresenter.class)));
        } else {
            goToBasketBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void navigateToLogin() {
        startActivity(new Intent(getContext(), LoginActivity.class));
        getActivity().finish();
    }

    @Override
    public void setMenuItem() {
        ((MainActivity) getActivity()).setMenuActiveItem(R.id.menu);
    }

    @Override
    public void setMenuList(List<DishUIModel> dishUIModelList) {
        dishAdapter.setDishUIModelList(dishUIModelList);
        dishAdapter.notifyDataSetChanged();
    }

    @Override
    public String getRouteTag() {
        return MAIN_TAG;
    }
}
