package shawarma.shawarma.ui.orderEditing.list;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import javax.inject.Inject;

import shawarma.shawarma.ShawarmaApp;
import shawarma.shawarma.data.model.Basket;
import shawarma.shawarma.data.model.dish.DishOrderUIModel;
import shawarma.shawarma.data.model.dish.DishUIModel;

@InjectViewState
public class OrderDishAdapterPresenter extends MvpPresenter<IOrderDishAdapterView> {

    @Inject
    Basket basket;

    public OrderDishAdapterPresenter() {
        ShawarmaApp.getBasketComponent().inject(this);
    }

    public List<DishOrderUIModel> getItems() {
        return basket.getItems();
    }

    public int minusItem(DishUIModel dishUIModel) {
        //return basket.minusItem(dishUIModel);
        return 0;
    }

    public int plusItem(DishUIModel dishUIModel) {
        return basket.plusItem(dishUIModel);
    }

    public void removeItem(DishUIModel dishUIModel) {
        basket.removeItem(dishUIModel);
    }
}
