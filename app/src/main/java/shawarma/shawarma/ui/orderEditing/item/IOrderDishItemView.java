package shawarma.shawarma.ui.orderEditing.item;

import com.arellomobile.mvp.MvpView;

public interface IOrderDishItemView extends MvpView {
    void setCount(int count);

    void setName(String name);
}
