package shawarma.shawarma.ui.orderEditing;

import com.arellomobile.mvp.MvpView;

public interface IOrderEditingView extends MvpView {
    void setTitle(String title);
}
