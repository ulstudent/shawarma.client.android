package shawarma.shawarma.ui.orderEditing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.ui.main.MainActivity;
import shawarma.shawarma.ui.orderDetails.OrderDetailActivity;
import shawarma.shawarma.ui.orderEditing.list.OrderDishAdapter;

public class OrderEditingActivity extends MvpAppCompatActivity implements IOrderEditingView {
    @InjectPresenter
    OrderEditingPresenter orderEditingPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitleView;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @BindView(R.id.clear)
    FloatingActionButton clearBtn;

    @BindView(R.id.go_to_menu)
    FloatingActionButton goToMenuBtn;

    @BindView(R.id.go_to_payment)
    FloatingActionButton goToPaymentBtn;

    @BindView(R.id.actions_menu)
    FloatingActionMenu actionMenuBtn;

    private OrderDishAdapter adapter = new OrderDishAdapter(getMvpDelegate());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_editing);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        orderEditingPresenter.setTitle(getString(R.string.order));

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        goToMenuBtn.setOnClickListener(v -> {
            closeMenu();
            navigateToMenu();
        });

        goToPaymentBtn.setOnClickListener(v -> {
            closeMenu();
            navigateToPayment();
        });

        clearBtn.setOnClickListener(v -> {
            closeMenuAnim();
            updateOrder();
        });
    }

    private void closeMenuAnim() {
        actionMenuBtn.close(true);
    }

    private void closeMenu() {
        actionMenuBtn.close(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(String title) {
        toolbarTitleView.setText(title);
    }

    public void navigateToMenu() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    public void navigateToPayment() {
        startActivity(new Intent(getApplicationContext(), OrderDetailActivity.class));
    }

    public void updateOrder() {
        orderEditingPresenter.clearBasket();
        adapter.notifyDataSetChanged();
    }

}
