package shawarma.shawarma.ui.orderEditing.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arellomobile.mvp.MvpDelegate;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;
import shawarma.shawarma.R;
import shawarma.shawarma.ShawarmaApp;
import shawarma.shawarma.base.MvpBaseAdapter;
import shawarma.shawarma.data.model.dish.DishUIModel;
import shawarma.shawarma.ui.orderEditing.item.IOrderDishItemView;
import shawarma.shawarma.ui.orderEditing.item.OrderDishItemPresenter;

public class OrderDishAdapter extends MvpBaseAdapter<OrderDishAdapter.OrderDishViewHolder>
        implements IOrderDishAdapterView {

    @InjectPresenter(type = PresenterType.WEAK, tag = "tag")
    OrderDishAdapterPresenter orderDishAdapterPresenter;

    public OrderDishAdapter(MvpDelegate<?> parentDelegate) {
        super(parentDelegate, String.valueOf(0));
    }

    @Override
    public OrderDishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_order_dish_item, parent, false);
        return new OrderDishViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderDishViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return orderDishAdapterPresenter.getItems().size();
    }

    public class OrderDishViewHolder extends RecyclerView.ViewHolder implements IOrderDishItemView {

        @BindView(R.id.name)
        TextView nameView;

        @BindView(R.id.count)
        TextView countView;

        @BindView(R.id.countInMenu)
        TextView countInMenuView;

        @BindView(R.id.itemSwipe)
        SwipeLayout swipeView;

        @BindView(R.id.countUpBtn)
        Button countUpBtn;

        @BindView(R.id.countDownBtn)
        Button countDownBtn;

        @BindView(R.id.deleteBtn)
        Button deleteBtn;

        private boolean isFirst = true;

        @InjectPresenter
        OrderDishItemPresenter orderDishItemPresenter;

        @ProvidePresenter
        OrderDishItemPresenter provideRepositoryPresenter() {
            return new OrderDishItemPresenter();
        }

        private MvpDelegate mMvpDelegate;

        public OrderDishViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            if (getMvpDelegate() != null) {
                getMvpDelegate().onSaveInstanceState();
                getMvpDelegate().onDetach();
                getMvpDelegate().onDestroyView();
                mMvpDelegate = null;
            }

            getMvpDelegate().onCreate();
            getMvpDelegate().onAttach();

            orderDishItemPresenter.setDishAndCount((Map.Entry<DishUIModel, Integer>)
                    orderDishAdapterPresenter.getItems().get(position));

            orderDishItemPresenter.setView();

            countDownBtn.setOnClickListener(v -> {
                int newValue = orderDishAdapterPresenter.minusItem(
                        orderDishItemPresenter.getDishAndCount().getKey());
                orderDishItemPresenter.setCount(newValue);
            });

            countUpBtn.setOnClickListener(v -> {
                int newValue = orderDishAdapterPresenter.plusItem(
                        orderDishItemPresenter.getDishAndCount().getKey());
                orderDishItemPresenter.setCount(newValue);
            });

            deleteBtn.setOnClickListener(v -> {
                orderDishAdapterPresenter.removeItem(
                        orderDishItemPresenter.getDishAndCount().getKey()
                );
                notifyItemRemoved(position);
            });
        }

        @Override
        public void setCount(int count) {
            countView.setText(ShawarmaApp.getAppContext()
                    .getResources()
                    .getString(R.string.item_count, count));
            countInMenuView.setText(String.valueOf(count));
        }

        @Override
        public void setName(String name) {
            nameView.setText(name);
        }

        MvpDelegate getMvpDelegate() {
            if (isFirst) {
                isFirst = false;
                return null;
            }
            if (mMvpDelegate == null) {
                mMvpDelegate = new MvpDelegate<>(this);
                mMvpDelegate.setParentDelegate(OrderDishAdapter.this.getMvpDelegate(), String.valueOf(0));

            }
            return mMvpDelegate;
        }
    }
}
