package shawarma.shawarma.ui.orderEditing;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import shawarma.shawarma.ShawarmaApp;
import shawarma.shawarma.data.model.Basket;

@InjectViewState
public class OrderEditingPresenter extends MvpPresenter<IOrderEditingView> {

    @Inject
    Basket basket;

    public OrderEditingPresenter() {
        ShawarmaApp.getBasketComponent().inject(this);
    }

    public void setTitle(String title) {
        getViewState().setTitle(title);
    }

    public void clearBasket() {
        basket.clear();
    }
}
