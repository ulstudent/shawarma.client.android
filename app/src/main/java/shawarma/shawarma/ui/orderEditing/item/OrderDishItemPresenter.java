package shawarma.shawarma.ui.orderEditing.item;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.Map;

import shawarma.shawarma.ShawarmaApp;
import shawarma.shawarma.data.model.dish.DishUIModel;

@InjectViewState
public class OrderDishItemPresenter extends MvpPresenter<IOrderDishItemView> {

    private Map.Entry<DishUIModel, Integer> dishAndCount;

    public Map.Entry<DishUIModel, Integer> getDishAndCount() {
        return dishAndCount;
    }

    public void setDishAndCount(Map.Entry<DishUIModel, Integer> dishAndCount) {
        this.dishAndCount = dishAndCount;
    }

    public void setView() {
        getViewState().setName(dishAndCount.getKey().getName());
        getViewState().setCount(dishAndCount.getValue());
    }

    public void setCount(int count) {
        dishAndCount.setValue(count);
        getViewState().setCount(dishAndCount.getValue());
    }

    public OrderDishItemPresenter() {
        ShawarmaApp.getBasketComponent().inject(this);
    }
}
