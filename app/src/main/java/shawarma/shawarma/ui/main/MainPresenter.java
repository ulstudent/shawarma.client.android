package shawarma.shawarma.ui.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import io.realm.Realm;
import shawarma.shawarma.data.model.database.UserDataModel;

@InjectViewState
public class MainPresenter extends MvpPresenter<IMainView> {
    public void setProfileNameToNavigation() {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> {
            UserDataModel userDataModel = realm.where(UserDataModel.class).findFirst();
            getViewState().setNameToNavigation(userDataModel.getName());
        });
    }

}
