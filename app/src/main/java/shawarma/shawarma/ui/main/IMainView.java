package shawarma.shawarma.ui.main;

import com.arellomobile.mvp.MvpView;

public interface IMainView extends MvpView {

    void setMenuActiveItem(int itemId);

    void setNameToNavigation(String name);
}
