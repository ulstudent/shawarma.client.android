package shawarma.shawarma.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.Pair;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.base.MvpFragment;
import shawarma.shawarma.common.utils.CircleTransform;
import shawarma.shawarma.ui.menu.MenuFragment;
import shawarma.shawarma.ui.orders.OrdersFragment;
import shawarma.shawarma.ui.settings.SettingsFragment;


public class MainActivity extends MvpAppCompatActivity implements IMainView {

    private String profile_picture_url = "https://pbs.twimg.com/profile_images/622184170572382208/1f-L5XGD.jpg";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigationView)
    NavigationView navigationView;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.toolbar_title)
    TextView toolbarText;

    @BindView(R.id.fragment_container)
    View container;

    @InjectPresenter
    MainPresenter mainPresenter;

    private ArrayMap<Integer, Pair<MvpFragment, String>> menuIndexToFragmentAndTitle
            = new ArrayMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        setNavigationMapMenuToIndex();
        setupNavigationDrawer();

        setupNavigationView();
        navigate(R.id.menu);
    }

    private void setNavigationMapMenuToIndex() {
        menuIndexToFragmentAndTitle.put(R.id.menu,
                new Pair<>(new MenuFragment(), getResources().getString(R.string.menu)));
        menuIndexToFragmentAndTitle.put(R.id.my_orders,
                new Pair<>(new OrdersFragment(), getResources().getString(R.string.my_orders)));
        menuIndexToFragmentAndTitle.put(R.id.settings,
                new Pair<>(new SettingsFragment(), getResources().getString(R.string.settings)));
    }

    private void setupNavigationView() {
        navigationView.setNavigationItemSelectedListener(item -> {
            navigate(item.getItemId());
            return true;
        });
    }

    private void navigate(int itemId) {
        setToolbarText(itemId);
        navigateToFragment(itemId);
        drawerLayout.closeDrawers();
    }

    private void navigateToFragment(final int menuItemId) {
        MvpFragment fragment
                = menuIndexToFragmentAndTitle.get(menuItemId).first;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (isMainFragmentNow()) {
            fragmentTransaction.addToBackStack(MvpFragment.MAIN_TAG);
        }
        fragmentTransaction.setCustomAnimations(R.anim.fragment_fade_in,
                R.anim.fragment_fade_out);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    private void setToolbarText(int menuItemId) {
        toolbarText.setText(menuIndexToFragmentAndTitle.get(menuItemId).second);
    }

    private void setupNavigationDrawer() {
        setNavigationDrawerView();
        setContentDisplace();
    }

    private void setContentDisplace() {
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.buy, R.string.cash) {
            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (navigationView.getWidth() * slideOffset);
                container.setTranslationX(moveFactor);
                toolbar.setTranslationX(moveFactor);
            }
        };

        drawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    private boolean isMainFragmentNow() {
        return getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container) instanceof MenuFragment;
    }

    private void setNavigationDrawerView() {
        navigationView.inflateHeaderView(R.layout.view_navigation_header);
        ImageView profile_image = (ImageView) navigationView.getHeaderView(0)
                .findViewById(R.id.nav_header_profile_image);

        Glide.with(this)
                .load(profile_picture_url)
                .transform(new CircleTransform(this))
                .into(profile_image);

        navigationView.setItemIconTintList(null);
        mainPresenter.setProfileNameToNavigation();
    }

    @Override
    public void setMenuActiveItem(int itemId) {
        //navigationView.getMenu().findItem(itemId).setChecked(true);
    }

    @Override
    public void setNameToNavigation(String name) {
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.nameTextView)).setText(name);
    }
}


