package shawarma.shawarma.ui.settings;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.base.MvpFragment;
import shawarma.shawarma.ui.views.MaskedEditText;

public class SettingsFragment extends MvpFragment implements ISettingsView {

    @InjectPresenter
    SettingsPresenter settingsPresenter;

    @BindView(R.id.name)
    EditText nameView;

    @BindView(R.id.phoneNumber)
    MaskedEditText phoneNumberView;

    @BindView(R.id.saveChanges)
    Button saveChangesBtn;

    @BindView(R.id.savedNotification)
    TextView savedNotificationView;

    private int notificationAnimDuration = 500;

    private int notificationShowDuration = 500;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        settingsPresenter.setNameAndPhone();

        saveChangesBtn.setOnClickListener(v -> {
            settingsPresenter.saveChanges();
            runNotificationAnim();
        });

        phoneNumberView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                settingsPresenter.setPhone(phoneNumberView.getRawText());
            }
        });

        nameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                settingsPresenter.setPhone(s.toString());
            }
        });
    }

    private void runNotificationAnim() {
        savedNotificationView.setAlpha(1);
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            Animation fadeOutAnim = new AlphaAnimation(1, 0);
            fadeOutAnim.setDuration(notificationAnimDuration);
            fadeOutAnim.setFillAfter(true);
            fadeOutAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    savedNotificationView.setAlpha(0);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            savedNotificationView.startAnimation(fadeOutAnim);
        }, notificationShowDuration);

    }

    @Override
    public void setNameAndPhone(String name, String phone) {
        nameView.setText(name);
        phoneNumberView.setText(phone);
    }


    @Override
    public String getRouteTag() {
        return ADDITIONAL_TAG;
    }
}
