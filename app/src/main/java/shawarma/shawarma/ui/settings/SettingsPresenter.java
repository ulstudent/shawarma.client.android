package shawarma.shawarma.ui.settings;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import io.realm.Realm;
import io.realm.RealmResults;
import shawarma.shawarma.data.model.database.UserDataModel;

@InjectViewState
public class SettingsPresenter extends MvpPresenter<ISettingsView> {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    private String phone;

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void saveChanges() {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> {
            final RealmResults<UserDataModel> userDataModel =
                    realm.where(UserDataModel.class).findAll();
            userDataModel.first().setName(name);
            userDataModel.first().setPhoneNumber(phone);
            realm.copyToRealmOrUpdate(userDataModel);
        });
    }

    public void setNameAndPhone() {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(realm -> {
            final RealmResults<UserDataModel> userDataModel =
                    realm.where(UserDataModel.class).findAll();
            if (!userDataModel.isEmpty()) {
                name = userDataModel.first().getName();
                phone = userDataModel.first().getPhoneNumber();
                getViewState().setNameAndPhone(userDataModel.first().getName(),
                        userDataModel.first().getPhoneNumber());
            }
        });
    }
}
