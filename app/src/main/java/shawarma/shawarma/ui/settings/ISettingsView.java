package shawarma.shawarma.ui.settings;

import com.arellomobile.mvp.MvpView;

public interface ISettingsView extends MvpView {
    void setNameAndPhone(String name, String phone);

}
