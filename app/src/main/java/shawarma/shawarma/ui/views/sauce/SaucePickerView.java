package shawarma.shawarma.ui.views.sauce;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import shawarma.shawarma.data.helper.AdditionalPositionHelper;
import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionPickerAdapterBase;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionPickerViewBase;

public class SaucePickerView extends AdditionalPositionPickerViewBase {

    public SaucePickerView(Context context) {
        super(context);
    }

    public SaucePickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SaucePickerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SaucePickerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected boolean isSuitableType(AdditionalPositionUIModel additionalPositionUIModel) {
        return AdditionalPositionHelper.isSauce(additionalPositionUIModel);
    }

    @Override
    protected AdditionalPositionPickerAdapterBase getAdapter() {
        return new SauceAdapter();
    }
}
