package shawarma.shawarma.ui.views.dishCounter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import shawarma.shawarma.R;

public class DishCounter extends LinearLayout {

    private DishCounterValueChangedListener listener;

    @BindView(R.id.counterText)
    TextView counterText;

    public DishCounter(Context context) {
        super(context);
    }

    public DishCounter(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DishCounter(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DishCounter(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @OnClick(R.id.plusDishButton)
    public void onPlusBtnClick() {
        int newValue = getCounterValue() + 1;
        counterText.setText(String.valueOf(newValue));
        setCounterValue(newValue);
        if (listener != null)
            listener.onValueChanged(newValue);
    }

    @OnClick(R.id.minusDishButton)
    public void onMinusBtnClick() {
        int newValue = getCounterValue() - 1;
        if (newValue > 0) {
            counterText.setText(String.valueOf(newValue));
            setCounterValue(newValue);
            if (listener != null)
                listener.onValueChanged(newValue);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    private int getCounterValue() {
        return Integer.valueOf(counterText.getText().toString());
    }

    private void setCounterValue(int newValue) {
        counterText.setText(String.valueOf(newValue));
    }

    public void setOnTextChangedListener(DishCounterValueChangedListener listener) {
        this.listener = listener;
    }
}
