package shawarma.shawarma.ui.views.pita;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import shawarma.shawarma.data.helper.AdditionalPositionHelper;
import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionPickerAdapterBase;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionPickerViewBase;


public class PitaPickerView extends AdditionalPositionPickerViewBase {
    public PitaPickerView(Context context) {
        super(context);
    }

    public PitaPickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PitaPickerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PitaPickerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected boolean isSuitableType(AdditionalPositionUIModel additionalPositionUIModel) {
        return AdditionalPositionHelper.isPita(additionalPositionUIModel);
    }

    @Override
    protected AdditionalPositionPickerAdapterBase getAdapter() {
        return new PitaAdapter();
    }
}
