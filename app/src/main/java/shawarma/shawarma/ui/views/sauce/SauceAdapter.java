package shawarma.shawarma.ui.views.sauce;

import shawarma.shawarma.R;
import shawarma.shawarma.data.helper.AdditionalPositionHelper;
import shawarma.shawarma.data.model.dish.AdditionalItemUIModel;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionPickerAdapterBase;

public class SauceAdapter extends AdditionalPositionPickerAdapterBase {

    @Override
    protected int getItemLayout() {
        return R.layout.view_sauce_item;
    }

    @Override
    protected String getImageUrl(AdditionalItemUIModel additionalItemUIModel) {
        return AdditionalPositionHelper.sauceNameToPicture.get(additionalItemUIModel.getName());
    }
}
