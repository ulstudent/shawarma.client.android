package shawarma.shawarma.ui.views.base.itemDetail;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;

public abstract class AdditionalPositionPickerViewBase extends LinearLayout {
    @BindView(R.id.leftBorder)
    public View leftBorder;

    @BindView(R.id.rightBorder)
    public View rightBorder;

    @BindView(R.id.listView)
    public RecyclerView recyclerView;

    @BindView(R.id.titleText)
    public TextView titleText;

    public AdditionalPositionPickerViewBase(Context context) {
        super(context);
    }

    public AdditionalPositionPickerViewBase(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AdditionalPositionPickerViewBase(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AdditionalPositionPickerViewBase(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    protected abstract boolean isSuitableType(AdditionalPositionUIModel additionalPositionUIModel);

    public void setAdditionalPositionData(AdditionalPositionUIModel additionalPosition) {
        if (!isSuitableType(additionalPosition)) {
            throw new RuntimeException("There must be additional position with suitable type");
        }

        titleText.setText(additionalPosition.getName());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
        AdditionalPositionPickerAdapterBase sauceAdapter = getAdapter();
        sauceAdapter.setItems(additionalPosition.getValues());
        sauceAdapter.setAdditionalPositionUIModel(additionalPosition);
        recyclerView.setAdapter(sauceAdapter);
    }

    protected abstract AdditionalPositionPickerAdapterBase getAdapter();

    public void setOnValueChangedListener(AdditionalPositionValueChangedListener listener) {
        if (recyclerView.getAdapter() != null)
        ((AdditionalPositionPickerAdapterBase) recyclerView.getAdapter()).setListener(listener);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }
}
