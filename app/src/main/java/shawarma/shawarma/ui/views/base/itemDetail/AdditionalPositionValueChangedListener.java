package shawarma.shawarma.ui.views.base.itemDetail;


import shawarma.shawarma.data.model.dish.AdditionalItemUIModel;
import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;

public abstract class AdditionalPositionValueChangedListener {
    public abstract void onValueAdded(AdditionalPositionUIModel additionalPositionUIModel,
                                      AdditionalItemUIModel additionalItemUIModel);

    public abstract void onValueRemoved(AdditionalPositionUIModel additionalPositionUIModel);

}
