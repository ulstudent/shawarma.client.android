package shawarma.shawarma.ui.views.pita;

import shawarma.shawarma.R;
import shawarma.shawarma.data.helper.AdditionalPositionHelper;
import shawarma.shawarma.data.model.dish.AdditionalItemUIModel;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionPickerAdapterBase;

public class PitaAdapter extends AdditionalPositionPickerAdapterBase {

    @Override
    protected int getItemLayout() {
        return R.layout.view_pita_item;
    }

    @Override
    protected String getImageUrl(AdditionalItemUIModel additionalItemUIModel) {
        return AdditionalPositionHelper.pitaNameToPicture.get(additionalItemUIModel.getName());
    }
}