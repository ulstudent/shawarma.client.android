package shawarma.shawarma.ui.views.dishCounter;


public abstract class DishCounterValueChangedListener {
    public abstract void onValueChanged(int value);
}
