package shawarma.shawarma.ui.views.base.itemDetail;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.AbstractList;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.common.utils.CircleTransform;
import shawarma.shawarma.data.model.dish.AdditionalItemUIModel;
import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;

public abstract class AdditionalPositionPickerAdapterBase extends
        RecyclerView.Adapter<AdditionalPositionPickerAdapterBase.ViewHolder> {

    private AdditionalPositionUIModel additionalPositionUIModel;
    private AbstractList<AdditionalItemUIModel> items;//= new ArrayList<>();

    private static final int DEFAULT_ACTIVE_POSITION = -1;
    private int activePosition = DEFAULT_ACTIVE_POSITION;

    private AdditionalPositionValueChangedListener listener;

    public void setAdditionalPositionUIModel(AdditionalPositionUIModel additionalPositionUIModel) {
        this.additionalPositionUIModel = additionalPositionUIModel;
    }

    public void setListener(AdditionalPositionValueChangedListener listener) {
        this.listener = listener;
    }

    public void setItems(AbstractList<AdditionalItemUIModel> items) {
        this.items = items;
    }

    protected abstract int getItemLayout();

    @Override
    public AdditionalPositionPickerAdapterBase.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(getItemLayout(), parent, false);

        return new AdditionalPositionPickerAdapterBase.ViewHolder(itemView);
    }

    protected abstract String getImageUrl(AdditionalItemUIModel additionalItemUIModel);

    @Override
    public void onBindViewHolder(AdditionalPositionPickerAdapterBase.ViewHolder holder, int position) {
        Glide.with(holder.itemImage.getContext())
                .load(getImageUrl(items.get(position)))
                .bitmapTransform(new CircleTransform(holder.itemImage.getContext()))
                .into(holder.itemImage);

        holder.itemWrapper.setBackgroundResource(position == activePosition ?
                R.drawable.sauce_item_active :
                0);

        holder.itemImage.setOnClickListener(v -> {
            int oldActivePosition = activePosition;

            if (listener != null && oldActivePosition != DEFAULT_ACTIVE_POSITION)
                listener.onValueRemoved(additionalPositionUIModel);

            if (position != activePosition) {
                activePosition = position;
                notifyItemChanged(oldActivePosition);
                notifyItemChanged(activePosition);
                if (listener != null)
                    listener.onValueAdded(additionalPositionUIModel, items.get(activePosition));
            } else {
                activePosition = DEFAULT_ACTIVE_POSITION;
                notifyItemChanged(oldActivePosition);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemImage)
        public ImageView itemImage;

        @BindView(R.id.itemWrapper)
        RelativeLayout itemWrapper;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

