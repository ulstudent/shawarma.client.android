package shawarma.shawarma.ui.orders;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import shawarma.shawarma.R;
import shawarma.shawarma.base.MvpFragment;

public class OrdersFragment extends MvpFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orders, container, false);
    }

    @Override
    public String getRouteTag() {
        return ADDITIONAL_TAG;
    }
}
