package shawarma.shawarma.ui.itemDetail;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;
import shawarma.shawarma.ui.orderDetails.OrderDetailActivity;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionValueChangedListener;
import shawarma.shawarma.ui.views.dishCounter.DishCounter;
import shawarma.shawarma.ui.views.dishCounter.DishCounterValueChangedListener;
import shawarma.shawarma.ui.views.pita.PitaPickerView;
import shawarma.shawarma.ui.views.sauce.SaucePickerView;

@StateStrategyType(AddToEndSingleStrategy.class)
public class ItemDetailActivity extends MvpAppCompatActivity
        implements IItemDetailView, AppBarLayout.OnOffsetChangedListener {

    public static final String ITEM_DISH_BUNDLE_KEY = "item_dish_bundle_key";

    @InjectPresenter
    ItemDetailPresenter itemDetailPresenter;

    @BindView(R.id.itemImage)
    ImageView itemImage;
    @BindView(R.id.costText)
    TextView costTextView;
    @BindView(R.id.goToMenuBtn)
    Button goToMenuButton;
    @BindView(R.id.buy_btn)
    Button buyButton;
    @BindView(R.id.goToPaymentBtn)
    Button goToPaymentBtn;
    @BindView(R.id.descriptionText)
    TextView descriptionText;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.detailImageGradient)
    View detailImageGradient;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.dishCounter)
    DishCounter dishCounter;
    @BindView(R.id.saucePicker)
    SaucePickerView saucePicker;
    @BindView(R.id.pitaPicker)
    PitaPickerView pitaPicker;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;

    private int appBarOffset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        ButterKnife.bind(this);

        itemDetailPresenter.setDishModelFromBundle(getIntent().getExtras());
        itemDetailPresenter.applyDishData();

        dishCounter.setOnTextChangedListener(new DishCounterValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                itemDetailPresenter.onCountChanged(value);
            }
        });

        buyButton.setOnClickListener(v -> addItemToBasket());

        goToMenuButton.setOnClickListener(v -> goToMenu());

        goToPaymentBtn.setOnClickListener(v -> goToPayment(v));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface originalTypeface = costTextView.getTypeface();
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedToolbarLayoutTitle);
        collapsingToolbarLayout.setExpandedTitleTypeface(originalTypeface);

        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsingToolbarLayoutTitle);
        collapsingToolbarLayout.setCollapsedTitleTypeface(originalTypeface);

        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (appBarOffset == 0) {
            finishAfterTransition();
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void addItemToBasket() {
        itemDetailPresenter.addItemToBasket();
    }

    @Override
    public void setBuyBtnText(int cost) {
        buyButton.setText(getString(R.string.buy, cost));
    }

    @Override
    public void loadImage(String url) {
        Glide.with(this)
                .load(url)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e,
                                               String model,
                                               Target<GlideDrawable> target,
                                               boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource,
                                                   String model,
                                                   Target<GlideDrawable> target,
                                                   boolean isFromMemoryCache,
                                                   boolean isFirstResource) {
                        detailImageGradient.startAnimation(AnimationUtils.loadAnimation(ItemDetailActivity.this,
                                R.anim.item_detail_image_gradient_fade_in));
                        return false;
                    }
                })
                .centerCrop()
                .crossFade()
                .into(itemImage);
    }

    @Override
    public void setName(String name) {
        collapsingToolbarLayout.setTitle(name);
    }

    public void setCostTextView(String text) {
        costTextView.setText(text);
    }

    @Override
    public void goToPayment(View v) {
        startActivity(new Intent(getApplicationContext(), OrderDetailActivity.class));
    }

    @Override
    public void goToMenu() {
        onBackPressed();
    }

    @Override
    public void showNavigateButtons() {
        buyButton.setVisibility(View.GONE);
        goToMenuButton.setVisibility(View.VISIBLE);
        goToPaymentBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void setDescription(String description) {
        descriptionText.setText(description);
    }

    @Override
    public void setSauceData(AdditionalPositionUIModel additionalPositionUIModel) {
        if (additionalPositionUIModel == null)
            saucePicker.setVisibility(View.GONE);
        else {
            saucePicker.setAdditionalPositionData(additionalPositionUIModel);
        }
    }

    @Override
    public void setPitaData(AdditionalPositionUIModel additionalPositionUIModel) {
        if (additionalPositionUIModel == null)
            pitaPicker.setVisibility(View.GONE);
        else {
            pitaPicker.setAdditionalPositionData(additionalPositionUIModel);
        }
    }

    @Override
    public void setListener(AdditionalPositionValueChangedListener additionalPositionValueChangedListener) {
        saucePicker.setOnValueChangedListener(additionalPositionValueChangedListener);
        pitaPicker.setOnValueChangedListener(additionalPositionValueChangedListener);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        appBarOffset = verticalOffset;
    }
}
