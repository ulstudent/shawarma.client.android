package shawarma.shawarma.ui.itemDetail;

import android.view.View;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionValueChangedListener;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface IItemDetailView extends MvpView {
    void setBuyBtnText(int cost);

    void loadImage(String url);

    void setName(String name);

    void setCostTextView(String text);

    void goToPayment(View v);

    void goToMenu();

    void showNavigateButtons();

    void setDescription(String description);

    void setSauceData(AdditionalPositionUIModel additionalPositionUIModel);

    void setPitaData(AdditionalPositionUIModel additionalPositionUIModel);

    void setListener(AdditionalPositionValueChangedListener additionalPositionValueChangedListener);
}
