package shawarma.shawarma.ui.itemDetail;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import shawarma.shawarma.ShawarmaApp;
import shawarma.shawarma.data.model.Basket;
import shawarma.shawarma.data.model.dish.AdditionalItemUIModel;
import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;
import shawarma.shawarma.data.model.dish.DishOrderBuilder;
import shawarma.shawarma.data.model.dish.DishUIModel;
import shawarma.shawarma.ui.views.base.itemDetail.AdditionalPositionValueChangedListener;

@InjectViewState
public class ItemDetailPresenter extends MvpPresenter<IItemDetailView> {

    @Inject
    Basket basket;

    private DishOrderBuilder dishOrderBuilder;

    private AdditionalPositionValueChangedListener additionalPositionValueChangedListener =
            new AdditionalPositionValueChangedListener() {
                @Override
                public void onValueAdded(AdditionalPositionUIModel additionalPositionUIModel,
                                         AdditionalItemUIModel additionalItemUIModel) {
                    dishOrderBuilder.addAdditionalPosition(additionalPositionUIModel, additionalItemUIModel);
                    getViewState().setBuyBtnText(dishOrderBuilder.getCommonCost());
                }

                @Override
                public void onValueRemoved(AdditionalPositionUIModel additionalPositionUIModel) {
                    dishOrderBuilder.removeAdditionalPosition(additionalPositionUIModel);
                    getViewState().setBuyBtnText(dishOrderBuilder.getCommonCost());
                }
            };

    public ItemDetailPresenter() {
        ShawarmaApp.getBasketComponent().inject(this);
    }

    public void setDishModelFromBundle(Bundle extras) {
        DishUIModel dishUIModel = (DishUIModel) extras.getSerializable(ItemDetailActivity.ITEM_DISH_BUNDLE_KEY);
        dishOrderBuilder = DishOrderBuilder.build(dishUIModel, null, null);
    }

    public void applyDishData() {
        getViewState().loadImage(dishOrderBuilder.getDishUIModel().getPictureUrl());
        getViewState().setName(dishOrderBuilder.getDishUIModel().getName());
        getViewState().setCostTextView(String.valueOf(dishOrderBuilder.getDishUIModel().getCost()));
        getViewState().setDescription(dishOrderBuilder.getDishUIModel().getDescription());
        getViewState().setSauceData(dishOrderBuilder.getDishUIModel().getSauceData());
        getViewState().setPitaData(dishOrderBuilder.getDishUIModel().getPitaData());
        getViewState().setListener(additionalPositionValueChangedListener);
    }

    public void onCountChanged(int count) {
        dishOrderBuilder.setCount(count);
        getViewState().setBuyBtnText(dishOrderBuilder.getCommonCost());
    }

    public void addItemToBasket() {
        /*if (count != 0)
            basket.addDish(dishOrderBuilder.getDishUIModel(), count);
*/
        getViewState().showNavigateButtons();
    }
}
