package shawarma.shawarma.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.common.Constants;
import shawarma.shawarma.ui.main.MainActivity;
import shawarma.shawarma.ui.views.MaskedEditText;

public class LoginActivity extends MvpAppCompatActivity implements ILoginView {

    @InjectPresenter
    LoginPresenter loginPresenter;

    @BindView(R.id.nameText)
    EditText nameText;
    @BindView(R.id.phoneText)
    MaskedEditText phoneText;
    @BindView(R.id.goOn)
    Button goOnBtn;

    private int minNameLength = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginPresenter.navigateInMenuIfNeed();

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        TextWatcher textWatcher = new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                checkBtnState();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        };

        checkBtnState();
        nameText.addTextChangedListener(textWatcher);
        phoneText.addTextChangedListener(textWatcher);

        goOnBtn.setOnClickListener(v ->
                loginPresenter.goOnBtnOnClick(nameText.getText().toString(),
                        phoneText.getRawText())
        );
    }

    private void checkBtnState() {
        if (nameText.getText().length() >= minNameLength
                && phoneText.getText().length() == Constants.phoneLength) {
            goOnBtn.setEnabled(true);
        } else {
            goOnBtn.setEnabled(false);
        }
    }

    @Override
    public void navigateToMenu() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
