package shawarma.shawarma.ui.login;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import shawarma.shawarma.data.model.database.UserDataModel;

@InjectViewState
public class LoginPresenter extends MvpPresenter<ILoginView> {

    Realm realm = Realm.getDefaultInstance();

    public void navigateInMenuIfNeed() {
        if (userInfoExists()) {
            getViewState().navigateToMenu();
        }
    }

    public void goOnBtnOnClick(String name, String phone) {
        addUserSettings(name, phone);
    }


    public boolean userInfoExists() {
        return !realm.where(UserDataModel.class).findAll().isEmpty();
    }

    public void addUserSettings(final String name, final String phone) {
        realm.executeTransaction(realm1 -> {
            final RealmResults<UserDataModel> userDataModel =
                    realm1.where(UserDataModel.class).findAll();
            UserDataModel dataModel;
            if (userDataModel.isEmpty()) {
                dataModel = realm1.createObject(UserDataModel.class, String.valueOf(UUID.randomUUID()));
            } else {
                dataModel = userDataModel.first();
            }
            dataModel.setName(name);
            dataModel.setPhoneNumber(phone);
            realm1.copyToRealmOrUpdate(dataModel);

            getViewState().navigateToMenu();
        });
    }
}
