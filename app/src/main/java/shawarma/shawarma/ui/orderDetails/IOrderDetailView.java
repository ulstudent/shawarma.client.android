package shawarma.shawarma.ui.orderDetails;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface IOrderDetailView extends MvpView {
    void setPhoneNumber(String phoneNumber);

    void setOrderSum(int sum);

    void setHours(int hours);

    void setMinutes(int minutes);

    void setAddressStreet(String street);

    void setAddressHouse(String house);

    void setCheckoutBtnActive();

    void setCheckoutBtnNotActive();

    void setTitle(String title);

    void navigateToOrderResult();

    void setLoaderVisible();

    void setLoaderInvisible();

    void setDelivery();

    void setPickup();
}
