package shawarma.shawarma.ui.orderDetails;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.Calendar;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import shawarma.shawarma.ShawarmaApp;
import shawarma.shawarma.common.Constants;
import shawarma.shawarma.data.model.Basket;
import shawarma.shawarma.data.model.database.UserDataModel;
import shawarma.shawarma.data.model.database.order.OrderModel;
import shawarma.shawarma.service.http.service.OrderHttpService;

@InjectViewState
public class OrderDetailPresenter extends MvpPresenter<IOrderDetailView> {

    @Inject
    Basket basket;

    private boolean isDelivery = true;

    private String deliveryStreet;

    private String deliveryHouse;

    private String phoneNumber;

    private int arriveHour;

    private int arriveMinute;

    private String pickupStreet = "Гончарова";
    private int pickupHouse = 20;

    private OrderHttpService orderHttpService = new OrderHttpService();

    public void setOrderType(boolean isDelivery) {
        this.isDelivery = isDelivery;
    }

    public void updateDeliveryOnView() {
        getViewState().setAddressStreet(deliveryStreet);
        getViewState().setAddressHouse(deliveryHouse);
    }

    public void deliveryBtnOnClick() {
        setOrderType(true);
        getViewState().setAddressHouse(deliveryHouse);
        getViewState().setAddressStreet(deliveryStreet);
        getViewState().setDelivery();
    }

    public void pickupBtnOnClick() {
        setOrderType(false);
        getViewState().setAddressHouse(String.valueOf(pickupHouse));
        getViewState().setAddressStreet(pickupStreet);
        getViewState().setPickup();
    }

    public void setArriveHour(int hour) {
        arriveHour = hour;
    }

    public void setArriveMinute(int minute) {
        arriveMinute = minute;
    }

    public OrderDetailPresenter() {
        ShawarmaApp.getBasketComponent().inject(this);
    }

    public void setBasketSum() {
        getViewState().setOrderSum(basket.getSum());
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPhoneNumber() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            final RealmResults<UserDataModel> userDataModel =
                    realm1.where(UserDataModel.class).findAll();
            if (!userDataModel.isEmpty()) {
                setPhoneNumber(userDataModel.first().getPhoneNumber());
                getViewState().setPhoneNumber(userDataModel.first().getPhoneNumber());
            }
        });
    }

    public void setDeliveryAddress(String street, String house) {
        deliveryStreet = street;
        deliveryHouse = house;
        updateDeliveryOnView();
    }

    public void setTimeToView(int minutesToCook) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, minutesToCook);
        getViewState().setHours(calendar.get(Calendar.HOUR_OF_DAY));
        getViewState().setMinutes(calendar.get(Calendar.MINUTE));
    }

    public void refreshCheckoutBtn() {
        if (isDelivery && deliveryStreet != null && !deliveryStreet.isEmpty() && phoneNumberFilled()) {
            getViewState().setCheckoutBtnActive();
        } else if (!isDelivery) {
            getViewState().setCheckoutBtnActive();
        } else {
            getViewState().setCheckoutBtnNotActive();
        }
    }

    private boolean phoneNumberFilled() {
        return phoneNumber != null && phoneNumber.length() == Constants.phoneRawLength;
    }

    public void setTitle(String title) {
        getViewState().setTitle(title);
    }

    public void makeOrder(String deviceImei) {
        OrderModel orderModel = new OrderModel();
        orderModel.setImei(deviceImei);
        orderModel.setOrderUuid(UUID.randomUUID().toString());
        orderHttpService.createOrder(orderModel)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().setLoaderVisible())
                .subscribe(orderResponseModel -> {
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(realm1 -> {
                        realm.copyToRealmOrUpdate(orderResponseModel);
                        getViewState().navigateToOrderResult();
                    });
                });
    }
}
