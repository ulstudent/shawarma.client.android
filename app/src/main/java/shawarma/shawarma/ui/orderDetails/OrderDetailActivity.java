package shawarma.shawarma.ui.orderDetails;


import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import shawarma.shawarma.R;
import shawarma.shawarma.ui.map.MapActivity;
import shawarma.shawarma.ui.orderResult.OrderResultActivity;
import shawarma.shawarma.ui.views.MaskedEditText;

public class OrderDetailActivity extends MvpAppCompatActivity implements IOrderDetailView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitleView;

    @BindView(R.id.deliveryBtn)
    Button deliveryBtn;

    @BindView(R.id.pickupBtn)
    Button pickupBtn;

    @BindView(R.id.takeAddress)
    TextView takeAddress;

    @BindView(R.id.currentStreet)
    TextView deliveryStreet;

    @BindView(R.id.currentHouse)
    TextView deliveryHouse;

    @BindView(R.id.phoneNumber)
    MaskedEditText phoneNumber;

    @BindView(R.id.courierWillArrive)
    TextView courierWillArrive;

    @BindView(R.id.willArrive)
    TextView willArrive;

    @BindView(R.id.timeArrive)
    View timeArriveBlock;

    @BindView(R.id.hours)
    NumberPicker hoursPicker;

    @BindView(R.id.minutes)
    NumberPicker minutesPicker;

    @BindView(R.id.checkoutBtn)
    Button checkoutBtn;

    @BindView(R.id.deliveryAddress)
    View deliveryAddress;

    @BindView(R.id.orderEditBtn)
    Button orderEditBtn;

    @BindView(R.id.loader)
    View loaderView;

    @InjectPresenter
    OrderDetailPresenter orderDetailPresenter;

    public static final String ADDRESS_MAP_KEY = "addressFragments";
    public static final int REQUEST_ADDRESS = 200;
    public static final int REQUEST_ORDER_EDIT = 201;

    private ArrayList<String> currentAddressFragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_order_detail);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        checkoutBtn.setOnClickListener(view -> {
            orderDetailPresenter.makeOrder(getDeviceImei());
        });

        orderDetailPresenter.setOrderType(true);
        setDelivery();

        orderDetailPresenter.setTitle(getString(R.string.payment));

        deliveryBtn.setOnClickListener(v -> orderDetailPresenter.deliveryBtnOnClick());
        pickupBtn.setOnClickListener(v -> orderDetailPresenter.pickupBtnOnClick());
        orderDetailPresenter.refreshCheckoutBtn();

        takeAddress.setOnClickListener(v ->
                startActivityForResult(new Intent(getApplicationContext(), MapActivity.class),
                        REQUEST_ADDRESS));

        orderEditBtn.setOnClickListener(v -> {
            new TimePickerDialog(OrderDetailActivity.this, (view, hourOfDay, minute) -> {

            }, 19, 17, false).show();
                /*startActivityForResult(new Intent(getApplicationContext(), OrderEditingActivity.class),
                        REQUEST_ORDER_EDIT)*/
        });

        orderDetailPresenter.setPhoneNumber();
        orderDetailPresenter.setBasketSum();

        orderDetailPresenter.setTimeToView(10);
    }

    private String getStreetOrTitle(String currentAddressFragment) {
        if (currentAddressFragment != null && !currentAddressFragment.isEmpty()) {
            return currentAddressFragments.get(0).split(",")[0];
        }
        return "";
    }


    @Override
    public void setDelivery() {
        courierWillArrive.setVisibility(View.VISIBLE);
        willArrive.setVisibility(View.GONE);
        takeAddress.setVisibility(View.VISIBLE);
        timeArriveBlock.setVisibility(View.GONE);
        deliveryBtn.setActivated(true);
        pickupBtn.setActivated(false);
    }

    @Override
    public void setPickup() {
        takeAddress.setVisibility(View.GONE);
        courierWillArrive.setVisibility(View.GONE);
        willArrive.setVisibility(View.VISIBLE);
        timeArriveBlock.setVisibility(View.VISIBLE);
        deliveryBtn.setActivated(false);
        pickupBtn.setActivated(true);
    }

    private String getHouse(String currentAddressFragment) {
        if (currentAddressFragment != null && !currentAddressFragment.isEmpty()) {
            String[] streetAndHouse = currentAddressFragment.split(",");
            if (streetAndHouse.length > 1)
                return streetAndHouse[1];
        }
        return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_ADDRESS:
                    currentAddressFragments = data.getStringArrayListExtra(ADDRESS_MAP_KEY);
                    orderDetailPresenter.setDeliveryAddress(
                            getStreetOrTitle(currentAddressFragments.get(0)),
                            getHouse(currentAddressFragments.get(0)));
                    orderDetailPresenter.refreshCheckoutBtn();
            }
        }
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.setText(phoneNumber);
    }

    public void setOrderSum(int sum) {
        orderEditBtn.setText(getString(R.string.order_view, sum));
    }

    @Override
    public void setHours(int hours) {
        hoursPicker.setMinValue(0);
        hoursPicker.setMaxValue(23);
        hoursPicker.setValue(hours);
    }

    @Override
    public void setMinutes(int minutes) {
        minutesPicker.setMinValue(0);
        minutesPicker.setMaxValue(59);
        minutesPicker.setValue(minutes);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setAddressStreet(String street) {
        deliveryStreet.setText(street);
    }

    @Override
    public void setAddressHouse(String house) {
        deliveryHouse.setText(house);
    }

    @Override
    public void setCheckoutBtnActive() {
        checkoutBtn.setEnabled(true);
    }

    @Override
    public void setCheckoutBtnNotActive() {
        checkoutBtn.setEnabled(false);
    }

    @Override
    public void setTitle(String title) {
        toolbarTitleView.setText(title);
    }

    @Override
    public void navigateToOrderResult() {
        startActivity(new Intent(getApplicationContext(), OrderResultActivity.class));
    }

    @Override
    public void setLoaderVisible() {
        loaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setLoaderInvisible() {
        loaderView.setVisibility(View.INVISIBLE);
    }

    private String getDeviceImei() {
        return ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
    }
}
