package shawarma.shawarma.data.model.dish;


import java.util.List;
import java.util.UUID;

import shawarma.shawarma.data.model.database.dish.AdditionalSelectedPositionModel;

public class DishOrderUIModel extends DishUIModel {

    private UUID uuid;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    private DishUIModel dishUIModel;

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private int commonSum;

    public int getCommonSum() {
        return commonSum;
    }

    public void setCommonSum(int commonSum) {
        this.commonSum = commonSum;
    }

    public DishOrderUIModel(DishUIModel dishUIModel,
                            List<AdditionalSelectedPositionModel> additionalSelectedPositionModels,
                            int count,
                            int commonSum,
                            UUID uuid) {
        this.dishUIModel = dishUIModel;
        this.additionalSelectedPositionModels = additionalSelectedPositionModels;
        this.count = count;
        this.commonSum = commonSum;
        this.uuid = uuid;
    }

    public DishUIModel getDishUIModel() {
        return dishUIModel;
    }

    public void setDishUIModel(DishUIModel dishUIModel) {
        this.dishUIModel = dishUIModel;
    }

    private List<AdditionalSelectedPositionModel> additionalSelectedPositionModels;

    public List<AdditionalSelectedPositionModel> getAdditionalSelectedPositionModels() {
        return additionalSelectedPositionModels;
    }

    public void setAdditionalSelectedPositionModels(List<AdditionalSelectedPositionModel> additionalSelectedPositionModels) {
        this.additionalSelectedPositionModels = additionalSelectedPositionModels;
    }
}
