package shawarma.shawarma.data.model.dish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import shawarma.shawarma.data.helper.AdditionalPositionHelper;

public class DishUIModel implements Serializable {

    private int id;

    private String name;

    private String description;

    private String pictureUrl;

    private int cost;

    private ArrayList<AdditionalPositionUIModel> additional;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public List<AdditionalPositionUIModel> getAdditional() {
        return additional;
    }

    public void setAdditional(ArrayList<AdditionalPositionUIModel> additional) {
        this.additional = additional;
    }

    public AdditionalPositionUIModel getSauceData() {
        if (additional == null)
            return null;
        for (AdditionalPositionUIModel additionalPositionUIModel : getAdditional()) {
            if (AdditionalPositionHelper.isSauce(additionalPositionUIModel))
                return additionalPositionUIModel;
        }
        return null;
    }

    public AdditionalPositionUIModel getPitaData() {
        if (additional == null)
            return null;
        for (AdditionalPositionUIModel additionalPositionUIModel : getAdditional()) {
            if (AdditionalPositionHelper.isPita(additionalPositionUIModel))
                return additionalPositionUIModel;
        }
        return null;
    }
}
