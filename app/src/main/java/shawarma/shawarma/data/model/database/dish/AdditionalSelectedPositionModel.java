package shawarma.shawarma.data.model.database.dish;


import io.realm.RealmObject;

public class AdditionalSelectedPositionModel extends RealmObject {
    private String type;

    private int cost;

    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
