package shawarma.shawarma.data.model.database.dish;


import io.realm.RealmObject;

public class AdditionalItemModel extends RealmObject {
    private String name;
    private Integer cost;

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
