package shawarma.shawarma.data.model.database.dish;

import io.realm.RealmList;
import io.realm.RealmObject;

public class AdditionalPositionModel extends RealmObject {
    private String name;

    private String type;

    private RealmList<AdditionalItemModel> values = new RealmList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public RealmList<AdditionalItemModel> getValues() {
        return values;
    }

    public void setValues(RealmList<AdditionalItemModel> values) {
        this.values = values;
    }
}
