package shawarma.shawarma.data.model;

import java.util.ArrayList;

import shawarma.shawarma.data.model.dish.DishOrderUIModel;
import shawarma.shawarma.data.model.dish.DishUIModel;

public class Basket {

    private ArrayList<DishOrderUIModel> items = new ArrayList<>();

    public ArrayList<DishOrderUIModel> getItems() {
        return items;
    }

    public void addDish(DishOrderUIModel dishUIModel) {
        items.add(dishUIModel);
    }

    public boolean isEmpty() {
        return items.size() == 0;
    }

    public Integer getSum() {
        int sum = 0;
        for (DishOrderUIModel dishOrderUIModel : items) {
            sum += dishOrderUIModel.getCommonSum();
        }
        return sum;
    }

    public int minusItem(DishOrderUIModel dishOrderUIModel) {

        /*for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getUuid().equals(dishOrderUIModel.getUuid())) {
                items.remove(i);
            }
        }*/
        return 0;
    }

    public int plusItem(DishUIModel dishUIModel) {
        /*if (items.get(dishUIModel) < 20)
            items.put(dishUIModel, items.get(dishUIModel) + 1);
        return items.get(dishUIModel);*/
        return 0;
    }

    public void removeItem(DishUIModel dishUIModel) {
        items.remove(dishUIModel);
    }

    public void clear() {
        items.clear();
    }

}
