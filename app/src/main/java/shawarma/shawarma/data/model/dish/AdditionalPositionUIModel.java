package shawarma.shawarma.data.model.dish;

import java.io.Serializable;
import java.util.ArrayList;

public class AdditionalPositionUIModel implements Serializable {
    private String name;

    private String type;

    private ArrayList<AdditionalItemUIModel> values;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<AdditionalItemUIModel> getValues() {
        return values;
    }

    public void setValues(ArrayList<AdditionalItemUIModel> values) {
        this.values = values;
    }
}
