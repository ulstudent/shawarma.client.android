package shawarma.shawarma.data.model.database.order;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class OrderModel extends RealmObject {
    @PrimaryKey
    private String orderUuid;

    private String imei;

    // private List<DishUIModel> dishes;

    public String getOrderUuid() {
        return orderUuid;
    }

    public void setOrderUuid(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

   /* public List<DishUIModel> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishUIModel> dishes) {
        this.dishes = dishes;
    }*/
}
