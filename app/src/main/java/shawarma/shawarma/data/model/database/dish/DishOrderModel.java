package shawarma.shawarma.data.model.database.dish;


import io.realm.RealmList;
import io.realm.RealmObject;

public class DishOrderModel extends RealmObject {

    private DishModel dishModel;

    public DishModel getDishModel() {
        return dishModel;
    }

    public void setDishModel(DishModel dishModel) {
        this.dishModel = dishModel;
    }

    private RealmList<AdditionalSelectedPositionModel> additionalSelectedPositionModels =
            new RealmList<>();

    public RealmList<AdditionalSelectedPositionModel> getAdditionalSelectedPositionModels() {
        return additionalSelectedPositionModels;
    }

    public void setAdditionalSelectedPositionModels(RealmList<AdditionalSelectedPositionModel> additionalSelectedPositionModels) {
        this.additionalSelectedPositionModels = additionalSelectedPositionModels;
    }
}
