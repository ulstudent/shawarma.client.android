package shawarma.shawarma.data.model.dish;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import shawarma.shawarma.data.model.database.dish.AdditionalSelectedPositionModel;

public class DishOrderBuilder {

    private int commonCost;

    private int count;

    private DishUIModel dishUIModel;

    public DishUIModel getDishUIModel() {
        return dishUIModel;
    }

    private UUID uuid;

    private List<AdditionalSelectedPositionModel> additionalSelectedPositionModels =
            new ArrayList<>();

    private DishOrderBuilder() {

    }

    public void addAdditionalPosition(AdditionalPositionUIModel additionalPositionUIModel,
                                      AdditionalItemUIModel additionalItemUIModel) {
        AdditionalSelectedPositionModel additionalSelectedPositionModel =
                new AdditionalSelectedPositionModel();
        additionalSelectedPositionModel.setName(additionalPositionUIModel.getName());
        additionalSelectedPositionModel.setType(additionalPositionUIModel.getType());
        additionalSelectedPositionModel.setCost(additionalItemUIModel.getCost());
        additionalSelectedPositionModels.add(additionalSelectedPositionModel);
        refreshCount();
    }

    public void removeAdditionalPosition(AdditionalPositionUIModel additionalPositionUIModel) {
        for (int i = 0; i < additionalSelectedPositionModels.size(); i++) {
            if (additionalSelectedPositionModels.get(i).getType()
                    .equals(additionalPositionUIModel.getType())) {
                additionalSelectedPositionModels.remove(i);
                return;
            }
        }
    }

    public static DishOrderBuilder build(DishUIModel dishUIModel,
                                         Integer count,
                                         List<AdditionalSelectedPositionModel> selectedPositions) {
        DishOrderBuilder dishOrderBuilder = new DishOrderBuilder();
        dishOrderBuilder.uuid = UUID.randomUUID();
        dishOrderBuilder.dishUIModel = dishUIModel;

        if (count != null)
            dishOrderBuilder.count = count;
        else
            dishOrderBuilder.count = 1;

        if (selectedPositions != null)
            dishOrderBuilder.additionalSelectedPositionModels = selectedPositions;
        else
            dishOrderBuilder.additionalSelectedPositionModels = new ArrayList<>();

        dishOrderBuilder.refreshCount();
        return dishOrderBuilder;
    }

    private void refreshCount() {
        int additionalSum = 0;
        for (AdditionalSelectedPositionModel model : additionalSelectedPositionModels) {
            additionalSum += model.getCost();
        }

        commonCost = count * (dishUIModel.getCost() + additionalSum);
    }

    public DishOrderUIModel getOrderModel() {
        return new DishOrderUIModel(dishUIModel, additionalSelectedPositionModels, count, commonCost, uuid);
    }

    public void setCount(int count) {
        this.count = count;
        refreshCount();
    }

    public int getCommonCost() {
        return commonCost;
    }
}
