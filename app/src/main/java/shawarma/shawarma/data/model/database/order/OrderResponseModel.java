package shawarma.shawarma.data.model.database.order;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class OrderResponseModel extends RealmObject {

    @PrimaryKey
    private String orderUuid;

    private String buyerName;

    private long orderTimestamp;

    private String hash;

    public String getOrderUuid() {
        return orderUuid;
    }

    public void setOrderUuid(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public long getOrderTimestamp() {
        return orderTimestamp;
    }

    public void setOrderTimestamp(long orderTimestamp) {
        this.orderTimestamp = orderTimestamp;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
