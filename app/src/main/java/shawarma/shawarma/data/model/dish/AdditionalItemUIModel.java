package shawarma.shawarma.data.model.dish;


import java.io.Serializable;

public class AdditionalItemUIModel implements Serializable{
    private String name;
    private Integer cost;

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
