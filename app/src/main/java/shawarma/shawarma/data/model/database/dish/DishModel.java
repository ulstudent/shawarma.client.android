package shawarma.shawarma.data.model.database.dish;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class DishModel implements RealmModel {
    private int id;

    private String name;

    private String description;

    private String pictureUrl;

    private int cost;

    private RealmList<AdditionalPositionModel> additional = new RealmList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public List<AdditionalPositionModel> getAdditional() {
        return additional;
    }

    public void setAdditional(RealmList<AdditionalPositionModel> additional) {
        this.additional = additional;
    }
}
