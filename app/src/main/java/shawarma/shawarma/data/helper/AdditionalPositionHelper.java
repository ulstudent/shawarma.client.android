package shawarma.shawarma.data.helper;

import java.util.HashMap;

import shawarma.shawarma.data.model.dish.AdditionalPositionUIModel;

public class AdditionalPositionHelper {

    private static final String SAUCE_TYPE = "sauce";
    private static final String PITA_TYPE = "pita";

    public static boolean isSauce(AdditionalPositionUIModel additionalPosition) {
        return additionalPosition.getType().equals(SAUCE_TYPE);
    }

    public static boolean isPita(AdditionalPositionUIModel additionalPosition) {
        return additionalPosition.getType().equals(PITA_TYPE);
    }

    public static HashMap<String, String> sauceNameToPicture;

    private static final String CHEESE_SAUCE = "Сырный";
    private static final String GARLIC_SAUCE = "Чесночный";
    private static final String SALSA_SAUCE = "Сальса";
    private static final String BARBECUE_SAUCE = "Барбекю";

    public static HashMap<String, String> pitaNameToPicture;

    private static final String CHEESE_PITA = "Сырный";
    private static final String NORMAL_PITA = "Обычный";
    private static final String GREEN_PITA = "Зеленый";

    static {
        sauceNameToPicture = new HashMap<>();
        sauceNameToPicture.put(GARLIC_SAUCE, "https://thumbs.dreamstime.com/x/%D1%87%D0%B5%D1%81%D0%BD%D0%BE%D0%BA-%D0%BD%D0%B0%D1%80%D0%B8%D1%81%D0%BE%D0%B2%D0%B0%D0%BD%D0%BD%D1%8B%D0%B9-%D1%80%D1%83%D0%BA%D0%BE%D0%B9-89632521.jpg");
        sauceNameToPicture.put(CHEESE_SAUCE, "https://st2.depositphotos.com/2935785/10774/v/450/depositphotos_107746200-stock-illustration-hand-drawn-piece-of-cheese.jpg");
        sauceNameToPicture.put(SALSA_SAUCE, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBKWuoG_vWLW5Duv1uWSaGGCEzn0fZTpOFsgAW_PP53Fx9b0pQ");
        sauceNameToPicture.put(BARBECUE_SAUCE, "http://www.highlandlakesaggies.com/wp-content/uploads/2017/01/BBQ.jpg");

        pitaNameToPicture = new HashMap<>();
        pitaNameToPicture.put(CHEESE_PITA, "https://16.img.avito.st/640x480/2688508516.jpg");
        pitaNameToPicture.put(NORMAL_PITA, "http://fitfan.ru/uploads/posts/2010-10/1287162616_lavash.jpg");
        pitaNameToPicture.put(GREEN_PITA, "http://smachno.stb.ua/wp-content/uploads/sites/25/2016/12/22/zK5E2eHakco.jpg");
    }
}
